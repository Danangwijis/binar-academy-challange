const readline = require("readline"); //mendeklarasi variable readline
const x = readline.createInterface({ //mendeklarasikan variable x
    input: process.stdin, //method untuk meminta input pada terminal
    output: process.stdout //method menghasilkan output dari syntax
});
console.log("Menghitung Akar Kuadrat")  //mencetak pada terminal string yang ada didalam console
    x.question("masukkan angka :", function(angka) { //method x.question mencetak string dan fungsi angka untuk menampung inputan pertama pada terminal
        console.log("Hasilnya Akar kuadrat = " +(Math.sqrt(+angka)));/*memanggil isi dari variable kemudian menghitungnya dengan rumus Math.sqrt, 
        (nilainya sama seperti akar kuadrat √) karena inputan sebelumnya adalah string maka diubah menjadi integer menggunakan +namavariable*/
        x.close(); //menutup method input dan output
    });