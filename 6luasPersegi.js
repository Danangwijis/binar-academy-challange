const readline = require("readline");//mendeklarasi variable readline
const x = readline.createInterface({ //mendeklarasikan variable x
    input: process.stdin, //method untuk meminta input pada terminal
    output: process.stdout //method menghasilkan output dari syntax
});
console.log("Menghitung Luas Persegi") //mencetak pada terminal string yang ada didalam console
    x.question("Masukkan Sisi:", function(sisi) { //method x.question mencetak string dan fungsi sisi untuk menampung inputan pertama pada terminal
        console.log("Luas Perseginya = " +(+sisi * 2)); /*memanggil isi dari variable kemudian mengkalinya, 
        (rumus dari luas persegi adalah sisi X sisi atau sisi X 2)karena inputan sebelumnya adalah string maka diubah menjadi integer menggunakan +namavariable*/
        x.close(); //menutup method input dan output
    });