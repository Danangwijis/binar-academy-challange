const readline = require("readline"); //mendeklarasi variable readline
const x = readline.createInterface({ //mendeklarasikan variable x
    input: process.stdin, //method untuk meminta input pada terminal
    output: process.stdout //method menghasilkan output dari syntax
});
console.log("Menghitung Volume Tabung") //mencetak pada terminal string yang ada didalam console
    x.question("Masukkan Jari - Jari :", function(jari) { //method x.question mencetak string dan fungsi jari untuk menampung inputan pertama pada terminal
    x.question("Masukkan Tinggi      :", function(tinggi) { //method x.question mencetak string dan fungsi tinggi untuk menampung inputan pertama pada terminal
        console.log("Hasilnya Penjumlahan  = " +(Math.PI * +jari * jari * +tinggi)); /*memanggil isi dari variable kemudian mengkalinya, 
        (rumus dari volume tabung adalah πr2t dimana π pada js menggunakan Math.PI)karena inputan sebelumnya adalah string maka diubah menjadi integer menggunakan +namavariable*/
        x.close(); //menutup method input dan output
    });
});